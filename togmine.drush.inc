<?php
/**
 * @file
 * TogMine drush command to execute TogMine for all valid accounts.
 */


/**
 * Debugs variables on the command line.
 *
 * @todo Minor: Move togmine_variable_export() to drush or devel?
 *
 * @param $variable
 *    The variable to debug.
 * @param $name
 *    The name of $variable, for labelled output.
 */
function togmine_variable_export($variable, $name = '') {
  static $counts = array();
  $counts[$name]++;
  $label = "$name {$counts[$name]}";
  print "\n===== Start debug: $label =====\n";
  var_export($variable);
  print "\n===== End debug: $label =====\n";
}

/**
 * An alias for drush_variable_export().
 *
 * @see drush_variable_export()
 */
function tve() {
  $args = func_get_args();
  return call_user_func_array('togmine_variable_export', $args);
}

/**
 * Implementation of hook_drush_command().
 */
function togmine_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['togmine-synchronize'] = array(
    'description' => 'Synchronizes Toggl tasks to Redmine time log entries.',
    'drupal dependencies' => array('togmine'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function togmine_drush_help($section) {
  switch ($section) {
    case 'meta:togmine:title': {
      return dt('TogMine commands');
    }
    case 'meta:togmine:summary': {
      // return dt('Synchronizes Toggl tasks to Redmine time log entries.');
      return dt('@TODO Where does this help string appear?');
    }
  }
}

/**
 * A drush command callback.
 */
function drush_togmine_synchronize() {
  $result = togmine_synchronize();

  // Save the result object for later use.
  variable_set('togmine_latest_result', $result);

  // Log the message to watchdog, which also outputs on the command line.
  // Drush prints watchdog messages to screen too.
  watchdog('togmine', $result->message, array(), !$result->success ? WATCHDOG_ERROR : ($result->warning ? WATCHDOG_WARNING : WATCHDOG_INFO));

  if (togmine_var('debug')) {
    // Log more detail in debug mode.
    // Don't repeat the message.
    unset($result->message);
    watchdog('togmine', '<pre>' . var_export($result, TRUE) . '</pre>', array(), WATCHDOG_DEBUG);
    // togmine_variable_export($result, 'TogMine Result');
  }
}
