<?php

function togmine_push_tickets($account) {
  static $tasks;
  if (!isset($tasks)) {
    $tasks = togmine_push_tickets_get_tasks();
    tve($tasks, 'tasks');
  }

  // @todo Two: Do this foreach task.
  $task = toggl_task_save($tasks[0]);
  // @todo Two: Save a reference to the task ID for this user and this issue ID.
}

function togmine_push_tickets_get_tasks() {
  $tasks = array();

  $sunday = date_create('last sunday');
  $workspace = new stdClass();
  // $workspace->id = togmine_var('toggl_workspace_id');
  $base_task = array(
    'duration' => 0,
    // @todo Two: Set this from the Redmine issue or project?
    'billable' => TRUE,
    'start' => $sunday->format(TOGGL_DATE_FORMAT),
    'workspace' => $workspace,
    'project' => new stdClass(),
    'planned_task' => new stdClass(),
    'created_with' => t('TogMine: Autocomplete Suggestions'),
  );

  $result = db_query('SELECT * FROM {togmine_projects}');
  while ($row = db_fetch_object($result)) {
    foreach (redmine_api_fetch_all('issues', array('project_id' => $row->redmine_project_id)) as $issue) {
      $t_args = array(
        '!id' => $issue->id,
        '!description' => $issue->subject,
      );

      $task = (Object) $base_task;
      // $task->project->id = $row->toggl_project_id;
      $task->description = t('#!id !description', $t_args);
      // $task->planned_task = @todo;
      $tasks[] = $task;
    }
  }

  return $tasks;
}
